import codecs
import re
import sys
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import dates as mdates
from datetime import datetime as dt
from enum import Enum
import glob
import os
import pandas as pd

# symbolごとに割り当てる値
symbol_index = pd.Series({
  'AUDCAD': 0,
  'AUDCHF': 1,
  'AUDJPY': 2,
  'AUDNZD': 3,
  'AUDUSD': 4,
  'CADJPY': 5,
  'CADCHF': 6,
  'CHFJPY': 7,
  'EURAUD': 8,
  'EURCAD': 9,
  'EURCHF': 10,
  'EURGBP': 11,
  'EURJPY': 12,
  'EURNZD': 13,
  'EURUSD': 14,
  'GBPAUD': 15,
  'GBPCAD': 16,
  'GBPCHF': 17,
  'GBPJPY': 18,
  'GBPNZD': 19,
  'GBPUSD': 20,
  'NZDCAD': 21,
  'NZDCHF': 22,
  'NZDJPY': 23,
  'NZDUSD': 24,
  'USDCAD': 25,
  'USDCHF': 26,
  'USDJPY': 27
})


# パスに存在するファイルで正規表現にマッチするものをリストで取得
def find_file(path, regx):
  match = glob.glob(path + regx)
  return match

# トレンド開始と終了の日時抽出
def extract_period(line):
  match = re.findall(r'\d+-\d+-\d+\s\d+:\d+:\d+', line)
  convert = np.zeros((1,0))
  for idx, value in enumerate(match):
    convert = np.append(convert, dt.strptime(value, '%Y-%m-%d %H:%M:%S'))
  return convert

# ファイルを読み込み特定幅のトレンドを抽出
def extract_trend(file, symbol, df, begin_date, end_date):
  match_count_300 = 0
  match_count_400 = 0
  match_count_500 = 0
  lines = file.readlines()
  for idx, line in enumerate(lines):
    match = re.search(r'Distance:\s((\+|-)\d+\.\d+)', line)
    if match:
      trend_width = float(match.group(1))
      if trend_width >= 500 or trend_width <= -500:
        period = extract_period(lines[idx-1])
        if (period[0] >= begin_date and period[0] <= end_date):
          match_count_500 = match_count_500 + 1
          df.loc[len(df)] = make_trend(symbol, period[0], period[1], trend_width, 500, match_count_500)
      elif trend_width >= 400 or trend_width <= -400:
        period = extract_period(lines[idx-1])
        if (period[0] >= begin_date and period[0] <= end_date):
          match_count_400 = match_count_400 + 1
          df.loc[len(df)] = make_trend(symbol, period[0], period[1], trend_width, 400, match_count_400)
      elif trend_width >= 300 or trend_width <= -300:
        period = extract_period(lines[idx-1])
        if (period[0] >= begin_date and period[0] <= end_date):
          match_count_300 = match_count_300 + 1
          df.loc[len(df)] = make_trend(symbol, period[0], period[1], trend_width, 300, match_count_300)

# 単体トレンド情報を生成
def make_trend(symbol, begin, end, width, size, count):
  s = pd.Series({'symbol': symbol, 'begin': begin, 'end': end, 'width': width, 'size': size, 'count': count})
  return s

# プロット
def plot_trend(df, img, width_thr):
  line_width = 10
  fig = plt.figure(figsize = (40, 20))
  ax = fig.add_subplot(111)
  ax.xaxis.set_major_formatter(mdates.DateFormatter('%Y-%m-%d'))
  ax.xaxis.set_major_locator(mdates.MonthLocator())
  for idx, trend in df.iterrows():
    if trend['width'] >= width_thr or trend['width'] <= -width_thr:
      if trend['width'] >= 0: # 上昇トレンドは寒色
        if trend['size'] == 300:
          ax.plot([trend['begin'], trend['end']], [symbol_index[trend['symbol']], symbol_index[trend['symbol']]], color = 'skyblue', lw = line_width)
        elif trend['size'] == 400:
          ax.plot([trend['begin'], trend['end']], [symbol_index[trend['symbol']], symbol_index[trend['symbol']]], color = 'dodgerblue', lw = line_width)
        elif trend['size'] == 500:
          ax.plot([trend['begin'], trend['end']], [symbol_index[trend['symbol']], symbol_index[trend['symbol']]], color = 'darkblue', lw = line_width)
      elif trend['width'] < 0: # 下降トレンドは暖色
        if trend['size'] == 300:
          ax.plot([trend['begin'], trend['end']], [symbol_index[trend['symbol']], symbol_index[trend['symbol']]], color = 'lightsalmon', lw = line_width)
        elif trend['size'] == 400:
          ax.plot([trend['begin'], trend['end']], [symbol_index[trend['symbol']], symbol_index[trend['symbol']]], color = 'tomato', lw = line_width)
        elif trend['size'] == 500:
          ax.plot([trend['begin'], trend['end']], [symbol_index[trend['symbol']], symbol_index[trend['symbol']]], color = 'crimson', lw = line_width)
  xlabel = ax.get_xticklabels()
  ax.set_yticks(symbol_index.values.tolist())
  ax.set_yticklabels(symbol_index.index.tolist())
  ax.set_facecolor('lightgrey')
  ax.grid(which = 'major', axis = 'y', color = 'white')
  plt.setp(xlabel, rotation=90)
  plt.show()
  fig.savefig(img, bbox_inches='tight')

# トレンド情報を一覧表示
def print_trends(df):
  for idx, trend in df.iterrows():
    print(trend)

args = sys.argv
if len(args) < 4:
  print("Usage")
  print("python3 trend_extraction.py [データが入っているディレクトリ名] [開始日] [終了日] [トレンド幅]")
  # e.g.) python3 trend_extraction.py M1_0_100 2016-1-1 2022-1-1 400
else:  
  dir_path = args[1]
  begin_date = dt.strptime(args[2], '%Y-%m-%d')
  end_date = dt.strptime(args[3], '%Y-%m-%d')
  trend_width = float(args[4])
  regx = "/*.txt"
  files = find_file(dir_path, regx) # トレンドデータファイル名のリスト
  df = pd.DataFrame(columns = ['symbol', 'begin', 'end', 'width', 'size', 'count'])
  for symbol_idx, file in enumerate(files):
    with codecs.open(file, "r", "UTF-16") as f:
      symbol = re.search(r'_(\w+)\.txt', file).group(1) # symbol名
      extract_trend(f, symbol, df, begin_date, end_date) 
  df = df.sort_values('size') # トレンドサイズの小さい順にソート
  img = "trend_analysis.png"
  plot_trend(df, img, trend_width)
  print_trends(df)